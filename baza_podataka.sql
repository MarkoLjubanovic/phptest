-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2017 at 03:07 PM
-- Server version: 5.6.17-log
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `baza_podataka`
--

-- --------------------------------------------------------

--
-- Table structure for table `knjige`
--

CREATE TABLE IF NOT EXISTS `knjige` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `korisnik_id` int(12) NOT NULL,
  `naziv` varchar(128) NOT NULL,
  `autor` varchar(128) NOT NULL,
  `godina_izdavanja` date NOT NULL,
  `jezik` varchar(128) NOT NULL,
  `org_jezik` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `knjige`
--

INSERT INTO `knjige` (`id`, `korisnik_id`, `naziv`, `autor`, `godina_izdavanja`, `jezik`, `org_jezik`) VALUES
(21, 24, 'Mali princ', ' 	Antoine de Saint-Exupéry', '1943-01-01', 'Srpski', 'francuski');

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE IF NOT EXISTS `korisnik` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `ime` varchar(128) NOT NULL,
  `prezime` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `sifra` int(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`id`, `ime`, `prezime`, `email`, `sifra`) VALUES
(24, 'Marko', 'Ljubanovic', 'markosss95@hotmail.rs', 123),
(26, 'jasmina', 'ljubanovic', 'jasmina.ljubanovic.7@facebook.com', 0),
(27, 'Sinisa', 'Ljubanovic', 'sinisaljubanovic76@gmail.com', 1234);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
